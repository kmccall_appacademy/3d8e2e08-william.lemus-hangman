require_relative 'hangman'

human_ref_hash = {guesser: ComputerPlayer.new(File.readlines('dictionary.txt')), referee: HumanPlayer.new}
human_guesser_hash = {referee: ComputerPlayer.new(File.readlines('dictionary.txt')), guesser: HumanPlayer.new}


puts "Welcome to Hangman."
puts "Enter 1 to be ref or 2 to be guesser"
role = gets.chomp
until role == "1" || role == "2"
  puts "Invalid choice, please try again"
  role = gets.chomp
end
game_hash = role == "1" ? human_ref_hash : human_guesser_hash
game = Hangman.new(game_hash)
game.setup
game.play
