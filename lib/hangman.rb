class Hangman
  attr_reader :guesser, :referee, :board
  def initialize(player_hash = {})
    @guesser = player_hash[:guesser]
    @referee = player_hash[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @board = Array.new(length)
    @guesser.register_secret_length(length)
  end

  def play
    take_turn while @board.any?(&:nil?)
    puts "Word guessed correctly!"
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(indices, guess) unless indices.empty?
    @guesser.handle_response(guess, indices)
    display_board
  end

  def update_board(indices, letter)
    indices.each { |idx| @board[idx] = letter }
  end

  def display_board
    @board.each do |c|
      if c.nil?
        print '_'
      else
        print c
      end
    end
    puts
  end
end

class HumanPlayer

  def pick_secret_word
    puts "Enter word"
    @secret_word = gets.chomp
    @secret_word.length

  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |c, idx|
      indices << idx if c == letter
    end
    indices
  end

  def register_secret_length(length)
    puts "word length is #{length}"
  end

  def guess(board)
    puts "enter a guess, human:"
    gets.chomp
  end

  def handle_response(letter, board) end
end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet_left = ("a".."z").to_a
  end

  def pick_secret_word
    @secret_word = @dictionary[rand(0...@dictionary.length)].chomp
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |c, idx|
      indices << idx if c == letter
    end

    indices
  end

  def register_secret_length(length)
    @secret_length = length
    @words_left = @dictionary.select { |word| word.chomp.length == @secret_length }
  end

  def guess(board)
    unless board.all?(&:nil?)
      board.each { |l| @alphabet_left.delete(l) unless l.nil? }
    end
    max_letter
  end

  def max_letter
    count = 0
    max_letter = ''
    all_letters = @words_left.join
    @alphabet_left.each do |letter|
      letter_freq = all_letters.count(letter)
      if letter_freq > count
        max_letter = letter
        count = letter_freq
      end
    end
    puts "ComputerPlayer guessed #{max_letter}"
    @alphabet_left.delete(max_letter)
  end

  def handle_response(letter, board)
    if board.empty?
      return @words_left.reject! do |word|
        word.include?(letter)
      end
    end
    @words_left.select! do |word|
      (0...word.length).all? do |idx|
        (board.include?(idx) && word[idx] == letter) || (!board.include?(idx) && word[idx] != letter)
      end
    end
  end

  def candidate_words
    @words_left
  end

end
